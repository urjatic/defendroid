package com.pikcn.defendroid.ui;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.preference.PreferenceManager;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.pikcn.defendroid.R;
import com.pikcn.defendroid.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {
    AppBarConfiguration mAppBarConfiguration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final ActivityMainBinding binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());


        setSupportActionBar(binding.appBarMain.toolbar);


        if (binding.appBarMain.fab != null) {
            binding.appBarMain.fab.setOnClickListener(view -> {
                NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
                navController.navigate(R.id.nav_scan);
            });
        }


        NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment_content_main);
        assert navHostFragment != null;
        NavController navController = navHostFragment.getNavController();

        // Portrait Mode //
        NavigationView navigationView = binding.navView;

        if (navigationView != null) {
            mAppBarConfiguration = new AppBarConfiguration.Builder(R.id.nav_apps_advisory, R.id.nav_settings_advisory, R.id.nav_preferences, R.id.nav_licences).setOpenableLayout(binding.drawerLayout).build();
            NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
            NavigationUI.setupWithNavController(navigationView, navController);
        }

        // Landscape Mode //
        final BottomNavigationView bottomNavigationView = binding.appBarMain.contentMain.bottomNavView;
        if (bottomNavigationView != null) {
            mAppBarConfiguration = new AppBarConfiguration.Builder(R.id.nav_apps_advisory, R.id.nav_settings_advisory).build();
            NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
            NavigationUI.setupWithNavController(bottomNavigationView, navController);
        }


        navController.addOnDestinationChangedListener(new MainDestinationChangedListener(binding));

        if (getCurrentNavDestinationId(navController) == R.id.nav_eula && PreferenceManager.getDefaultSharedPreferences(this).getBoolean("eula_accepted", false)) {
            // EULA is already accepted, show scan page //
            navController.navigate(R.id.action_eula_to_scan);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        boolean result = super.onCreateOptionsMenu(menu);
        // Using findViewById because NavigationView exists in different layout files
        // between w600dp and w1240dp
        NavigationView navView = findViewById(R.id.nav_view);
        if (navView == null) {
            // The navigation drawer already has the items including the items in the overflow menu
            // We only inflate the overflow menu if the navigation drawer isn't visible
            getMenuInflater().inflate(R.menu.overflow, menu);
        }
        return result;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        int currentId = getCurrentNavDestinationId();
        menu.setGroupVisible(R.id.group_options_menu, currentId != R.id.nav_eula);
        return super.onPrepareOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        final NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        int currentId = getCurrentNavDestinationId(navController);
        if (currentId != R.id.nav_eula) {
            if (item.getItemId() == R.id.nav_preferences) {
                navController.navigate(R.id.nav_preferences);
            }
            if (item.getItemId() == R.id.nav_licences) {
                navController.navigate(R.id.nav_licences);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        final boolean result = super.onSupportNavigateUp();
        int currentId = getCurrentNavDestinationId();
        if (currentId == R.id.nav_scan || currentId == R.id.nav_eula) {
            // No Back Support for Scan and Eula Fragments
            return false;
        }

        return NavigationUI.navigateUp(Navigation.findNavController(this, R.id.nav_host_fragment_content_main), mAppBarConfiguration) || result;
    }

    private int getCurrentNavDestinationId() {
        return getCurrentNavDestinationId(Navigation.findNavController(this, R.id.nav_host_fragment_content_main));
    }

    private int getCurrentNavDestinationId(NavController navController) {
        NavDestination current = navController.getCurrentDestination();
        if (current != null) {
            return current.getId();
        }
        return 0;
    }
}