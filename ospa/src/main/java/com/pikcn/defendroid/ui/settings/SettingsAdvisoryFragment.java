package com.pikcn.defendroid.ui.settings;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pikcn.defendroid.app.ScanWorker;
import com.pikcn.defendroid.databinding.FragmentSettingsAdvisoryListBinding;

public class SettingsAdvisoryFragment extends Fragment {
    public SettingsAdvisoryFragment() {
    }

    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {

        @NonNull final FragmentSettingsAdvisoryListBinding binding = FragmentSettingsAdvisoryListBinding.inflate(inflater, container, false);
        final View root = binding.getRoot();

        final RecyclerView recyclerView = binding.list;
        recyclerView.setLayoutManager(new LinearLayoutManager(inflater.getContext()));
        recyclerView.setAdapter(new SettingsAdvisoryRecyclerViewAdapter(ScanWorker.getSettingsScanResults()));

        return root;
    }

}