package com.pikcn.defendroid.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.pikcn.defendroid.databinding.FragmentOpenLicensesBinding;

public class OpenLicensesFragment extends Fragment {
    private FragmentOpenLicensesBinding binding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentOpenLicensesBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.licenceView.clearHistory();
        binding.licenceView.loadUrl("file:///android_asset/dependency-license.html");
    }
}