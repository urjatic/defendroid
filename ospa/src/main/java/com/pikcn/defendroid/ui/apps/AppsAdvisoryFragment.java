package com.pikcn.defendroid.ui.apps;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.pikcn.defendroid.app.ScanWorker;
import com.pikcn.defendroid.databinding.FragmentAppsAdvisoryListBinding;

public class AppsAdvisoryFragment extends Fragment {

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final FragmentAppsAdvisoryListBinding binding = FragmentAppsAdvisoryListBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        RecyclerView recyclerView = binding.list;
        recyclerView.setLayoutManager(new LinearLayoutManager(inflater.getContext()));
        recyclerView.setAdapter(new AppsAdvisoryRecyclerViewAdapter(ScanWorker.getAppsScanResults()));
        return root;
    }
}