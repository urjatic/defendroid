package com.pikcn.defendroid.ui.apps;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pikcn.defendroid.app.AppsScanResult;
import com.pikcn.defendroid.databinding.FragmentAppsAdvisoryBinding;

public class AppsAdvisoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public final ImageView mIcon;
    public final TextView mTitle;
    public final TextView mDescription;
    public AppsScanResult mItem;

    public AppsAdvisoryViewHolder(FragmentAppsAdvisoryBinding binding) {
        super(binding.getRoot());
        binding.getRoot().setOnClickListener(this);
        mTitle = binding.appAdvName;
        mIcon = binding.appAdvIcon;
        mDescription = binding.appAdvDesc;
    }


    @NonNull
    @Override
    public String toString() {
        return super.toString() + " '" + mTitle.getText() + " ~ " + "'";
    }

    @Override
    public void onClick(View v) {
        try {
            mItem.onClick(v);
        } catch (Exception e) {
            Log.d(AppsAdvisoryViewHolder.class.getCanonicalName(), "Unexpected error in View.OnClickListener", e);
        }
    }
}
